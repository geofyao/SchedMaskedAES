#include <QCoreApplication>
#include <iostream>
#include <string>
#include "tools.h"
#include "superscheduler.h"
using namespace std;


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    vector<vector<int>> in(4,vector<int>(4));
    vector<vector<int>> key(4,vector<int>(4));

    map<string,vector<vector<int>>> conf=Tools::readFile("C:\\Users\\J\\Desktop\\schedTest\\in\\conf1.txt");
    in=conf.at("plain");
    key=conf.at("key");

    cout<<"plaintext"<<endl;
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            cout<<to_string(in.at(i).at(j))<<" " ;
        }
        cout<<endl;
    }
cout<<endl;

    cout<<"key"<<endl;
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            cout<<to_string(key.at(i).at(j))<<" " ;
        }
        cout<<endl;
    }

    SuperScheduler sched(in,key,4);
    sched.execute();
    vector<vector<int>> cipherText=sched.displayFinalCipher();


    cout<<"ciphertext"<<endl;
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            cout<<to_string(cipherText.at(i).at(j))<<" " ;
        }
        cout<<endl;
    }

    return a.exec();
}


