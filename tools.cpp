#include "tools.h"
#include <iostream>
#include <vector>
#include <algorithm>    // std::find
#include <string>
#include <cstdlib>      // std::rand, std::srand
#include <fstream>
#include <time.h>
using namespace std;
Tools::Tools()
{

}

int Tools::split(vector<string>& vecteur, string chaine, char separateur){
    vecteur.clear();

    string::size_type stTemp = chaine.find(separateur);

    while(stTemp != string::npos)
    {
        vecteur.push_back(chaine.substr(0, stTemp));
        chaine = chaine.substr(stTemp + 1);
        stTemp = chaine.find(separateur);
    }

    vecteur.push_back(chaine);

    return vecteur.size();
}

void Tools::moveToLeft(std::vector<int>& vecteur){
    vector<int>tmp(vecteur);
    for(unsigned i=1;i<tmp.size();i++){
        vecteur[i-1]=tmp[i];
    }
    vecteur[tmp.size()-1]=tmp[0];
}

int Tools::myrandom (int i) { return std::rand()%i;}

map<string,vector<vector<int>>> Tools:: readFile(string fileName){
      string line;
      map<string,vector<vector<int>>> conf;
      ifstream myfile (fileName);
      string current="";
      if (myfile.is_open())
      {
        while ( getline (myfile,line) )
        {
          if(line.find("plaintext")!=line.npos||line.find("key")!=line.npos){
               if(line.find("plaintext")!=line.npos)
                    current="plain";
               else if(line.find("key")!=line.npos)
                    current="key";
              vector<vector<int>>vec(4,vector<int>(4,0));
              vector<vector<string>>x;
              for(int i=0;i<4;i++){
                  getline (myfile,line);
                  vector<string>tmp;
                  split(tmp,line,' ');
                  x.push_back(tmp);
              }

              for(unsigned i=0;i<x.size();i++){
                  for(unsigned j=0;j<x.size();j++){
                      vec.at(i).at(j)=stoi(x.at(i).at(j));
                  }
                  conf[current]=vec;
              }
          }
        }
        myfile.close();
      }

      else cout << "Unable to open file";

      return conf;

}
void Tools::exportTasksToFile(ofstream* fichier, string str) {
    if (fichier->is_open())
    {
        *fichier<<str<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << *fichier << "'" << endl;
}
void Tools::exportTasksToFile( ofstream* fichier, vector<vector<vector<int>>> vec,string title) {
    if (fichier->is_open())
    {
        *fichier<<title<<endl;
        for(unsigned i=0;i<vec.size();i++){
            for(unsigned j=0;j<vec.at(i).size();j++){
                for(unsigned z=0;z<vec.at(i).at(j).size();z++)
                    *fichier<<to_string(vec.at(i).at(j).at(z))<<" ";
            }
            *fichier<<endl;
        }
         *fichier<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << fichier << "'" << endl;
}
void Tools::exportTasksToFile( ofstream* fichier, vector<int> vec,string title) {
    if (fichier->is_open())
    {
        *fichier<<title<<endl;
        for(unsigned i=0;i<vec.size();i++){
            *fichier<<to_string(vec.at(i))<<" ";
        }
         *fichier<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << *fichier << "'" << endl;
}
void Tools::exportTasksToFile( ofstream* fichier, vector<string> vec,string title) {
    if (fichier->is_open())
    {
        *fichier<<title<<endl;
        for(unsigned i=0;i<vec.size();i++){
            *fichier<<vec.at(i)<<" ";
        }
         *fichier<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << *fichier << "'" << endl;
}
void Tools::exportTasksToFile(ofstream* fichier, vector<vector<int>> vec,string title) {
    if (fichier->is_open())
    {
        *fichier<<title<<endl;
        for(unsigned i=0;i<vec.size();i++){
            for(unsigned j=0;j< vec.at(i).size();j++){
                *fichier<<to_string(vec.at(i).at(j))<<" ";
            }
            *fichier<<endl;
        }
         *fichier<<endl;
    }
    else
        cerr << "Couldn't write to outputfile '" << *fichier << "'" << endl;
}


void Tools::closeFile(ofstream *fichier){
    fichier->close();
}


bool check(const std::vector<string>& vec, string str)
{
    auto iter=find(vec.begin(), vec.end(), str);
    return iter!= vec.end();
}



void Tools::myShuffle(vector<string> &array)
{// Use a different seed value so that we don't get same
    // result each time we run this program
    srand ( time(NULL) );

    // Start from the last element and swap one by one. We don't
    // need to run for the first element that's why i > 0
    for (int i = array.size()-1; i > 0; i--)
    {
        // Pick a random index from 0 to i
        int j = rand() % (i+1);

        // Swap arr[i] with the element at random index
        swap(array[i], array[j]);
    }
}
