#include "superscheduler.h"
#include <sstream>
#include <iostream>
#include <random>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <algorithm>
#include <chrono>
#include "schedaes.h"
#include "tools.h"
using namespace std;
SuperScheduler::SuperScheduler()
{

}

SuperScheduler::SuperScheduler(vector<vector<int>> plaintext, vector<vector<int>> key,unsigned order):
    d{order},nbLines{4},nbCol{4},nbRounds{10}{
    srand(chrono::system_clock::now().time_since_epoch().count());
    vector<vector<vector<int>>> shares=setAllShares(plaintext);
    vector<vector<vector<int>>> keysShares=setAllShares(key);
    this->file= new ofstream("C:\\Users\\J\\Desktop\\schedTest\\out.txt",ios::app);
    for(unsigned i=0;i<=d;i++){
        this->allMatrices.push_back(new SchedAES(vector<vector<int>>(shares.at(i)),vector<vector<int>>(keysShares.at(i)),i));

    }
    K=vector<vector<vector<int>>>(allMatrices.size(),vector<vector<int>>(4,vector<int>(4,0)));
    C=vector<vector<int>>(allMatrices.size(),vector<int>(4,0));
    SB= vector<vector<int>>(4,vector<int>(4,0));
    R=vector<int>(4,0);
    M=vector<vector<vector<int>>>(allMatrices.size(),vector<vector<int>>(4,vector<int>(4,-1)));

    /*vector<string> startSet = {"ARK0-0-0", "ARK0-0-1", "ARK0-0-2", "ARK0-0-3",
                       "ARK0-1-0", "ARK0-1-1", "ARK0-1-2","ARK0-1-3",
                       "ARK0-2-0", "ARK0-2-1", "ARK0-2-2", "ARK0-2-3",
                       "ARK0-3-0", "ARK0-3-1", "ARK0-3-2","ARK0-3-3",
                       "KE1-0-0", "KE1-1-0", "KE1-2-0", "KE1-3-0"};*/

    vector<string> startSet = {"ARK0-0-0", "ARK0-0-1", "ARK0-0-2", "ARK0-0-3",
                       "ARK0-1-0", "ARK0-1-1", "ARK0-1-2","ARK0-1-3",
                       "ARK0-2-0", "ARK0-2-1", "ARK0-2-2", "ARK0-2-3",
                       "ARK0-3-0", "ARK0-3-1", "ARK0-3-2","ARK0-3-3"};

    //on rajoute ici dans un seul activeSet toutes les opérations de départ pour chacun de shares

        for(unsigned j=0;j<startSet.size();j++){
             for(unsigned i=0;i<=d;i++){
                 this->activeSet.push_back(to_string(i)+"-"+startSet.at(j));
            }
        }    
    previousOk=std::vector<std::map<int,std::vector<bool>>>(allMatrices.size());
    keSbReady[0]=true;
    for(unsigned i=1;i<=nbRounds;i++){
        keSbReady[i]=false;
    }

    for(unsigned j=0;j<allMatrices.size();j++){

        for(unsigned i=0;i<=nbRounds;i++){
            previousOk.at(j)[i]=vector<bool>(4,false);
        }
        for(int i=0;i<4;i++){
           previousOk.at(j).at(0).at(i)=true;
        }
    }
    computeKeySchedule();

}

void SuperScheduler::computeKeySchedule(){
    vector<string> tmp={"KE1-0-0", "KE1-1-0", "KE1-2-0", "KE1-3-0"};
    for(unsigned i=0;i<allMatrices.size();i++){
        for(unsigned j=0;j<tmp.size();j++){
            keySet.push_back(to_string(i)+"-"+tmp.at(j));
        }
    }

    while(!keySet.empty()){
        string action="";
        action +=keySet.at(0);
        Tools::exportTasksToFile(file,action+" selected");
        keySet.erase(keySet.begin());
        perform_ActionAndUpdate(action);
    }
    cout<<"Key expansion over"<<endl;
}

vector<vector<vector<int>>> SuperScheduler::setAllShares(vector<vector<int>> matrice){

    vector<vector<vector<int>>> shares;
    shares.push_back(matrice);
    for(unsigned k=0;k<d;k++){
        vector<vector<int>> vec(createRandomShare());
        shares.push_back(vec);
        for(unsigned i=0;i<nbLines;i++){
            for(unsigned j=0;j<nbCol;j++){
                shares.at(0).at(i).at(j) = shares.at(0).at(i).at(j) ^ vec.at(i).at(j);
            }
        }
    }

    return shares;

}

vector<vector<int>> SuperScheduler:: createRandomShare(int high,int low ){


    vector<vector<int>>vec(4,vector<int>(4));
    for(unsigned i=0;i<nbLines;i++){
        for(unsigned j=0;j<nbCol;j++){
            vec.at(i).at(j)=rand() % (high - low + 1) + low;
        }
    }
    return vec;
}

string SuperScheduler:: pick_ActionR(){
    if(!activeSet.empty()){
        string action="";
        unsigned seed = chrono::system_clock::now().time_since_epoch().count();
        shuffle ( activeSet.begin(), activeSet.end(),default_random_engine(seed) );
        action +=activeSet.at(0);
        Tools::exportTasksToFile(file,action+" selected");
        activeSet.erase(activeSet.begin());
        return action;
    }
    return "";
}

void SuperScheduler::perform_ActionAndUpdate(string action){

    int k = 0;
    if(action.empty())
        return;
    vector<string> actionParts;
    Tools::split(actionParts,action,'-');
    string op,tmp;
    if((action.find("SR")!=action.npos) || (action.find("MC")!=action.npos)){

        if(action.find("SR")!=op.npos){
            int i = stoi(actionParts[1]);
            op = actionParts[0];
            tmp=op.size()>3?string(op,op.size()-2,2):string(1,op.at(op.size()-1));
            k=stoi(tmp);
            R.at(i)+=1;
            for(unsigned matrice=0;matrice<allMatrices.size();matrice++){// il faut absolument appliquer le shiftrow en meme temps sur chaque matrice
                allMatrices.at(matrice)->shiftRows(i);
            }
            srUpdate(k,i);
        }
        else if(action.find("MC")!=op.npos){
            op = actionParts[1];
            int matrice=stoi(actionParts[0]);
            int i = stoi(actionParts[2]);
            tmp=op.size()>3?string(op,op.size()-2,2):string(1,op.at(op.size()-1));
            k=stoi(tmp);
            C.at(matrice).at(i) +=1;
            allMatrices.at(matrice)->mixColumns(k,i,vector<int>(R));
            mcUpdate(matrice,k,i);

        }
    }
    else{

        if( action.find("ARK")!=op.npos){
            int matrice=stoi(actionParts[0]);
            int j = stoi(actionParts[3]);
            int i = stoi(actionParts[2]);
            op = actionParts[1];
            tmp=op.size()>4?string(op,op.size()-2,2):string(1,op.at(op.size()-1));
            k = stoi (tmp);
            M[matrice][i][j]+=1;
            allMatrices.at(matrice)->addRoundKey(k,i,j,vector<int>(R));
            arkUpdate(k,i,j);

        }
        else if(action.find("SB")!=op.npos){//on check s'il est possible de realiser le subByte! il faut que la meme opération soit prete pour tous les shares            
            int j = stoi(actionParts[2]);
            int i = stoi(actionParts[1]);
            op = actionParts[0];
            tmp=op.size()>3?string(op,op.size()-2,2):string(1,op.at(op.size()-1));
            k = stoi (tmp);
            SB[i][j]+=1;
            doSuByte(i,j,k,false);
            sbUpdate(k,i,j);
        }
        else if(action.find("KE")!=op.npos){
            int matrice=stoi(actionParts[0]);
            int j = stoi(actionParts[3]);
            int i = stoi(actionParts[2]);
            op = actionParts[1];
            tmp=op.size()>3?string(op,op.size()-2,2):string(1,op.at(op.size()-1));
            k = stoi(tmp);
            if(j==0 ){

                if( !keSbReady.at(k)){
                        int indice_j= (((j-1)%nbCol)+nbCol)%nbCol;
                        for(unsigned line=0;line<nbLines;line++){
                            doSuByte(line,indice_j,k-1,true);//on fait le subByte pour l'entiere colonne
                        }
                          sbPerformed(k);

                }
            }
            K[matrice][i][j] +=1;
            allMatrices.at(matrice)->secKeyExpansion(k,i,j);
            keUpdate(matrice,k,i,j);

        }
    }

}


void SuperScheduler::addAction(string action,bool newAction){
    auto iter=find(activeSet.begin(), activeSet.end(), action);
    if( iter== activeSet.end()){//add action only if it didn't exist before in the set
        activeSet.push_back(action);
        if(newAction)
            Tools::exportTasksToFile(file,action+" added");
        else
            Tools::exportTasksToFile(file,action+" again");
    }

}



void SuperScheduler::doSuByte(int i, int j,int k,bool isKe){
    vector<int> vec(allMatrices.size());
   for(unsigned z=0;z<allMatrices.size();z++){
       vec.at(z)=allMatrices.at(z)->getElt(i,j,k,isKe);
   }
   vec=SchedAES::secSbox(vec);
    for(unsigned z=0;z<allMatrices.size();z++){
        allMatrices.at(z)->setElt(i,j,vec.at(z),k,isKe);
    }
}

bool SuperScheduler::isPreviousReady(int k){
  for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
    for(int i=0;i<4;i++){
        if(!previousOk.at(matrice).at(k).at(i))
            return false;
    }
  }
    return true;
}

void SuperScheduler::sbPerformed(int k){

        keSbReady.at(k)=true;
}

void SuperScheduler::updateOpList(vector<string>& toAdd, vector<string>& toRemove, string cmp,string str){
    auto iter=find(toRemove.begin(), toRemove.end(), cmp);
    if( iter!= toRemove.end()){
        toRemove.erase(iter);
        toAdd.push_back(str);
        Tools::exportTasksToFile(file,cmp+" removed after sr");
    }
}

void SuperScheduler::keUpdate(int matrice,int k, int i, int j){
    //# 1ere partie on autorise le KE suivant
    //# cas particulier car pour la colonne suivante on autorise avec
    //# un decalage d’une ligne (voir keyscheduling)
    if (j == 3){
        int indice=(((i - 1) % 4)+4)%4;
        previousOk.at(matrice).at(k).at(i)=true;
        if ((K[matrice][indice][0] >= k) && (k != 10)){
            /*if(isPreviousReady(k)){
                for(unsigned line=0;line<nbLines;line++){
                    doSuByte(line,3,k,true);//on fait le subByte pour l'entiere colonne
                }
                  sbPerformed(k);

                for(unsigned mat=0;mat<allMatrices.size();mat++){
                    for(unsigned u=0;u<allMatrices.size();u++){
                       string str=to_string(mat)+"-KE" + to_string(k + 1) + "-" + to_string(u) + "-" + to_string(0);
                       activeSet.push_back(str);
                       Tools::exportTasksToFile(file,str+" added");
                    }
                }

            }*/
            /*if(isPreviousReady(k) ){
                for(unsigned line=0;line<nbLines;line++){
                    doSuByte(line,3,k,true);//on fait le subByte pour l'entiere colonne
                }
                  sbPerformed(k);
            }*/
            string str=to_string(matrice)+"-KE" + to_string(k + 1) + "-" + to_string(indice) + "-" + to_string(0);
            keySet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
    }
    else{
        int indice=(((j - 3) % 4)+4)%4;
        if (K[matrice][i][indice] >= (k - 1)){
            string str=to_string(matrice)+"-KE" + to_string(k) + "-" + to_string(i) + "-" + to_string((j + 1) % 4);
            keySet.push_back(str);
            //activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
    }
    if (j == 0){
        if ((K[matrice][(i + 1) % 4][3] >= k) && (k != 10)){
            string str=to_string(matrice)+"-KE" + to_string(k + 1) + "-" + to_string(i) + "-" + to_string(0);
            keySet.push_back(str);
            //activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
    }
    else{
        if ((K[matrice][i][(j + 3) % 4] >= k + 1) && (k != 10)){
            string str=to_string(matrice)+"-KE" + to_string(k + 1) + "-" + to_string(i) + "-" + to_string(j);
            keySet.push_back(str);
            //activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
    }
    // # 2eme partie on regarde si il faut autoriser un ARK
    // # (celui pour le KE que l’on vient de faire)
    // # il faut voir aussi si les autres conditions
    // # pour ajouter sont respectee
    if (k != 10){
        if ((M[matrice][i][j] < k) && (C[matrice][j] == k)){
            if (R[i] == k){
                string str=to_string(matrice)+"-ARK" + to_string(k) + "-" + to_string(i) + "-" + to_string(j);
                activeSet.push_back(str);
                Tools::exportTasksToFile(file,str+" added");
            }
            else if (R[i] == k - 1){
                string str=to_string(matrice)+"-ARK" + to_string(k) + "-" + to_string(i) + "-" + to_string((j + i) % 4);
                activeSet.push_back(str);
                Tools::exportTasksToFile(file,str+" added");
            }
        }
    }
    else{
        int newj=-1;
        if (R[i] == k){
            newj = j;
        }
        else if (R[i] == k - 1){
            newj = (j + i) % 4;
        }
        else if (R[i] == k - 2){
            newj = (j + (2 * i)) % 4;
        }
        if (newj!=-1 && (SB[i][newj] == k) && (M[matrice][i][newj] < k)){
            string str=to_string(matrice)+"-ARK" + to_string(k) + "-" + to_string(i) + "-" + to_string(newj);
            activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
    }
}

void SuperScheduler::sbUpdate(int k, int i, int j){
    if (k != nbRounds){
        int virtual_j = 0;
        if (R[i] == k){
            virtual_j = j;
        }
        else{
            virtual_j = (((j - i) % 4)+4)%4;
        }
        if (((SB[0][virtual_j] == k) && (R[0] == k)) || (
                               (SB[0][virtual_j] == k) && (R[0] == k - 1))){
                       if (((SB[1][virtual_j] == k) && (R[1] == k)) || (
                                   (SB[1][(virtual_j + 1) % 4] == k) && (R[1] == k - 1))){
                           if (((SB[2][virtual_j] == k) && (R[2] == k)) || (
                                       (SB[2][(virtual_j + 2) % 4] == k) && (R[2] == k - 1))){
                               if (((SB[3][virtual_j] == k) && (R[3] == k)) || (
                                           (SB[3][(virtual_j + 3) % 4] == k) && (R[3] == k - 1))){
                        for(int matrice=0;matrice<allMatrices.size();matrice++){
                            string str=to_string(matrice)+"-MC" + to_string(k) + "-" + to_string(virtual_j);
                            activeSet.push_back(str);
                            Tools::exportTasksToFile(file,str+" added");
                        }
                    }
                }
            }
        }
    }
    //# au dernier tour
    //# si la case est alignee (SR10 fait) et la cle est prete (K10-i-j fait)
    //# ou si il manque un SR et que la cle est prete, mais l’indice pour la cle represente la case de cle qui correspond a l’alignement
    else if (k == nbRounds){
        for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
            if ( ((R[i] == k) && (K[matrice][i][j] >= k)) || (
                         (R[i] == k - 1) && (K[matrice][i][((((j - i) % 4)+4)%4)] >= k))){
                string str=to_string(matrice)+"-ARK" + to_string(k) + "-" + to_string(i) + "-" + to_string(j);
                activeSet.push_back(str);
                Tools::exportTasksToFile(file,str+" added");
            }
        }

    }
}

void SuperScheduler:: arkUpdate(int k, int i, int j){
    if (k <= nbRounds - 1){
        bool allAlike=true;

        bool srOk=true;
        for(unsigned id=0;id<allMatrices.size();id++){ // ici on va vérifier que tous les ark dans toutes les matrices ont été fait
            if (srOk && !((M[id][i][0] >= k) && (M[id][i][1] >= k) && (M[id][i][2] >= k) && (
                        M[id][i][3] >= k) && (R[i] == k))){
                srOk=false;
            }
            if(allAlike && M[id][i][j]!=k){
                allAlike=false;
            }
        }
        if(allAlike){
            string str="SB" + to_string(k + 1) + "-" + to_string(i) + "-" + to_string(j);
            activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }
        if (srOk){

            string str="SR" + to_string(k + 1) + "-" + to_string(i);
            activeSet.push_back(str);
            Tools::exportTasksToFile(file,str+" added");
        }

    }
}


void SuperScheduler::mcUpdate(int matrice,int k, int j){
    string str;
    if (K[matrice][0][j] >= k){
        str=to_string(matrice)+"-ARK" + to_string(k) + "-0-" + to_string(j);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    if ((R[1] == k) && (K[matrice][1][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-1-" + to_string(j);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    else if ((R[1] == k - 1) && (K[matrice][1][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-1-" + to_string((j + 1) % 4);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    if ((R[2] == k) && (K[matrice][2][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-2-" + to_string(j);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    else if ((R[2] == k - 1) && (K[matrice][2][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-2-" + to_string((j + 2) % 4);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    if ((R[3] == k) && (K[matrice][3][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-3-" + to_string(j);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
    else if ((R[3] == k - 1) && (K[matrice][3][j] >= k)){
        str.clear();
        str=to_string(matrice)+"-ARK" + to_string(k) + "-3-" + to_string((j + 3) % 4);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }
}


void SuperScheduler::srUpdate(int k, int i){
    //# partie 1 on decale les donnees pour la ligne dans l’ActiveSet et les matrices M et SB

    vector<string>tempSet;
    vector<int>tmp1(SB[i]);    
    for(unsigned j=0;j<4;j++){
        int indice=(((j-i)%4)+4)%4;
        for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
            updateOpList(tempSet,activeSet,(to_string(matrice)+"-ARK"+to_string(k-1)+"-"+to_string(i)+"-"+to_string(j)),to_string(matrice)+"-ARK"+to_string(k-1)+"-"+to_string(i)+"-"+to_string(indice));
            updateOpList(tempSet,activeSet,(to_string(matrice)+"-ARK"+to_string(k)+"-"+to_string(i)+"-"+to_string(j)),to_string(matrice)+"-ARK"+to_string(k)+"-"+to_string(i)+"-"+to_string(indice));
            updateOpList(tempSet,activeSet,(to_string(matrice)+"-ARK"+to_string(k+1)+"-"+to_string(i)+"-"+to_string(j)),to_string(matrice)+"-ARK"+to_string(k+1)+"-"+to_string(i)+"-"+to_string(indice));
        }
        updateOpList(tempSet,activeSet,("SB"+to_string(k-1)+"-"+to_string(i)+"-"+to_string(j)),"SB"+to_string(k-1)+"-"+to_string(i)+"-"+to_string(indice));
        updateOpList(tempSet,activeSet,("SB"+to_string(k)+"-"+to_string(i)+"-"+to_string(j)),("SB"+to_string(k)+"-"+to_string(i)+"-"+to_string(indice)));
        updateOpList(tempSet,activeSet,("SB"+to_string(k+1)+"-"+to_string(i)+"-"+to_string(j)),("SB"+to_string(k+1)+"-"+to_string(i)+"-"+to_string(indice)));
         SB[i][j]=tmp1[(j+i)%4];

    }

    for(unsigned cpt=0;cpt<tempSet.size();cpt++){
        activeSet.push_back(tempSet.at(cpt));
        Tools::exportTasksToFile(file,tempSet.at(cpt)+" added");
    }

    for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
         vector<int>tmp(M[matrice][i]);
        for(unsigned j=0;j<4;j++){
            M[matrice][i][j]=tmp[(j+i)%4];
        }
    }
    //# partie 2 on regarde si on peut autoriser des MC
    if (k + 1 != nbRounds){
        for(int virtual_j=0;virtual_j<4;virtual_j++){
            if (((SB[0][virtual_j] == k + 1) && (R[0] == k + 1)) || (
                        (SB[0][virtual_j] == k + 1) && (R[0] == k))){
                if (((SB[1][virtual_j] == k + 1) && (R[1] == k + 1)) || (
                            (SB[1][(virtual_j + 1) % 4] == k + 1) && (R[1] == k))){
                    if (((SB[2][virtual_j] == k + 1) && (R[2] == k + 1)) || (
                                (SB[2][(virtual_j + 2) % 4] == k + 1) && (R[2] == k))){
                        if (((SB[3][virtual_j] == k + 1) && (R[3] == k + 1)) || (
                                    (SB[3][(virtual_j + 3) % 4] == k + 1) && (R[3] == k))){
                            for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
                                string str=to_string(matrice)+"-MC" + to_string(k + 1) + "-" + to_string(virtual_j);
                                auto iter=find(activeSet.begin(), activeSet.end(), str);
                                if( iter== activeSet.end()&& C[matrice][virtual_j] == k){
                                    activeSet.push_back(str);
                                    Tools::exportTasksToFile(file,str+" added");
                                }
                            }

                        }
                    }
                }
            }
        }
    }
    //   # partie 3 on regarde si on peut autoriser des SR
    bool srOk=true;
    for(unsigned matrice=0;matrice<allMatrices.size();matrice++){ // ici on va vérifier que tous les ark dans toutes les matrices ont été fait
        if (!((M[matrice][i][0] >= k) && (M[matrice][i][1] >= k) && (M[matrice][i][2] >= k) && (
                    M[matrice][i][3] >= k) && (k != nbRounds))){
            srOk=false;
            break;
        }
    }

    if (srOk){
        string str="SR" + to_string(k + 1) + "-" + to_string(i);
        activeSet.push_back(str);
        Tools::exportTasksToFile(file,str+" added");
    }

    //  # partie 4 on regarde si on peut autoriser des ARK
    if ((k + 1) == nbRounds){
        for(unsigned matrice=0;matrice<allMatrices.size();matrice++){
            for(int j=0;j<4;j++){
                if (((SB[i][j] == k + 1) && (K[matrice][i][j] == k + 1))){
                    string str=to_string(matrice)+"-ARK" + to_string(k + 1) + "-" + to_string(i) + "-" + to_string(j);
                    activeSet.push_back(str);
                    Tools::exportTasksToFile(file,str+" added");
                }
            }

        }
    }

}

void SuperScheduler::step(){
    this->perform_ActionAndUpdate(pick_ActionR());
    for(unsigned i=0;i<allMatrices.size();i++){
        Tools::exportTasksToFile(file,M[i],to_string(i)+"-M:");       
        Tools::exportTasksToFile(file,C[i],to_string(i)+"-C:");
        Tools::exportTasksToFile(file,K[i],to_string(i)+"-K:");
    }
    Tools::exportTasksToFile(file,SB,"SB:");
    Tools::exportTasksToFile(file,R,"R:");
    Tools::exportTasksToFile(file   ,activeSet,"ActiveSet = ");
}

bool SuperScheduler::onlyKEinActiveSet(){

    for(unsigned i=0; i<activeSet.size();i++){
        if(activeSet.at(i).find("KE")==activeSet.at(i).npos )//On doit avoir KE en fait
            return false;

    }
    return true;
}


bool SuperScheduler:: finish(){
    if ((activeSet.size() == 0) || this->onlyKEinActiveSet()){
        Tools::closeFile(file);
        return true;
    }else{
        return false;
    }
}

void SuperScheduler:: execute(){
    while(!finish()){
        step();
    }
    cout<<"fin"<<endl;
}


vector<vector<int>> SuperScheduler::displayFinalCipher(){
    vector<vector<int>> vec=allMatrices.at(0)->getCipherText();

    for(unsigned i=1;i<allMatrices.size();i++){
        vector<vector<int>> cipher_i=allMatrices.at(i)->getCipherText();
        for(unsigned z=0;z<cipher_i.size();z++){
            for(unsigned j=0;j<cipher_i.at(z).size();j++){
                vec.at(z).at(j)^=cipher_i.at(z).at(j);

            }
        }
    }
    return vec;
}
