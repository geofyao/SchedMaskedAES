#ifndef SCHEDAES_H
#define SCHEDAES_H

#include <vector>
#include<stdlib.h>
#include <string>

class SchedMaskedController;
class SchedAESControl;
class SchedAES
{
public:
    SchedAES();
    SchedAES(std::vector<std::vector<int>> plaintext , std::vector<std::vector<int>> key,int indice,int line=4, int col=4, int rounds=10);
    int getNbLines();
    int getNbColumn();
    int getNbRounds();
    bool secKeyExpansion(int k, int i, int j);
    void subBytes(int , int );
    void shiftRows( int);
    void mixColumns(int, int,std::vector<int>R);
    void addRoundKey(int k, int i, int j,std::vector<int>R);
    std::vector<std::vector<int>>getCipherText();
    static std::vector<int> secSbox(std::vector<int> shares);
    int getElt(int i, int j,int k,bool isKe);
    void setElt(int i, int j, int elt,int k,bool isKe);
    static int af(int x);
    static std::vector<int> secExp254(std::vector<int>& x);
private:
    int nbLines;
    int nbCol;
    int nbRounds;
    int indiceShare;
    std::vector<int> sBox;
    std::vector<int> rcon;
    std::vector<int> mCM2;
    std::vector<int> mCM3;
    std::vector<std::vector<int>> plainText;
    std::vector<std::vector<int>> cipherText;
    std::vector<std::vector<int>> state;
    std::vector<std::vector<std::vector<int>>> roundKeys;
    std::vector<std::vector<int>> t; //sert au secKeyExpansion
    SchedMaskedController* parent;
    void initRoundKey( std::vector<std::vector<int>> );
    static std::vector<int> secMult(std::vector<int>& a,std::vector<int>& b);
    static void refreshMask(std::vector<int>& x);
    static uint8_t gmul(uint8_t a, uint8_t b);
    static std::vector<bool>intToBit(int b);


};

#endif // SCHEDAES_H
