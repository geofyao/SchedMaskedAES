#include "schedaes.h"
#include <iostream>
#include "tools.h"
#include <stdlib.h>
#include <time.h>
#include <cmath>
using namespace std;

SchedAES::SchedAES(){}
SchedAES::SchedAES( std::vector<std::vector<int>> plaintext,
                    std::vector<std::vector<int>> key,int indice,int line,int col,int rounds):indiceShare{indice},nbLines{line},nbCol{col},nbRounds{rounds}
{
    this->rcon= {NULL, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36};

    this->mCM2 = {
        0x00, 0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c, 0x0e, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e,
        0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e,
        0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x5e,
        0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 0x76, 0x78, 0x7a, 0x7c, 0x7e,
        0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c, 0x8e, 0x90, 0x92, 0x94, 0x96, 0x98, 0x9a, 0x9c, 0x9e,
        0xa0, 0xa2, 0xa4, 0xa6, 0xa8, 0xaa, 0xac, 0xae, 0xb0, 0xb2, 0xb4, 0xb6, 0xb8, 0xba, 0xbc, 0xbe,
        0xc0, 0xc2, 0xc4, 0xc6, 0xc8, 0xca, 0xcc, 0xce, 0xd0, 0xd2, 0xd4, 0xd6, 0xd8, 0xda, 0xdc, 0xde,
        0xe0, 0xe2, 0xe4, 0xe6, 0xe8, 0xea, 0xec, 0xee, 0xf0, 0xf2, 0xf4, 0xf6, 0xf8, 0xfa, 0xfc, 0xfe,
        0x1b, 0x19, 0x1f, 0x1d, 0x13, 0x11, 0x17, 0x15, 0x0b, 0x09, 0x0f, 0x0d, 0x03, 0x01, 0x07, 0x05,
        0x3b, 0x39, 0x3f, 0x3d, 0x33, 0x31, 0x37, 0x35, 0x2b, 0x29, 0x2f, 0x2d, 0x23, 0x21, 0x27, 0x25,
        0x5b, 0x59, 0x5f, 0x5d, 0x53, 0x51, 0x57, 0x55, 0x4b, 0x49, 0x4f, 0x4d, 0x43, 0x41, 0x47, 0x45,
        0x7b, 0x79, 0x7f, 0x7d, 0x73, 0x71, 0x77, 0x75, 0x6b, 0x69, 0x6f, 0x6d, 0x63, 0x61, 0x67, 0x65,
        0x9b, 0x99, 0x9f, 0x9d, 0x93, 0x91, 0x97, 0x95, 0x8b, 0x89, 0x8f, 0x8d, 0x83, 0x81, 0x87, 0x85,
        0xbb, 0xb9, 0xbf, 0xbd, 0xb3, 0xb1, 0xb7, 0xb5, 0xab, 0xa9, 0xaf, 0xad, 0xa3, 0xa1, 0xa7, 0xa5,
        0xdb, 0xd9, 0xdf, 0xdd, 0xd3, 0xd1, 0xd7, 0xd5, 0xcb, 0xc9, 0xcf, 0xcd, 0xc3, 0xc1, 0xc7, 0xc5,
        0xfb, 0xf9, 0xff, 0xfd, 0xf3, 0xf1, 0xf7, 0xf5, 0xeb, 0xe9, 0xef, 0xed, 0xe3, 0xe1, 0xe7, 0xe5
    };
    this->mCM3 = {
        0x00, 0x03, 0x06, 0x05, 0x0c, 0x0f, 0x0a, 0x09, 0x18, 0x1b, 0x1e, 0x1d, 0x14, 0x17, 0x12, 0x11,
        0x30, 0x33, 0x36, 0x35, 0x3c, 0x3f, 0x3a, 0x39, 0x28, 0x2b, 0x2e, 0x2d, 0x24, 0x27, 0x22, 0x21,
        0x60, 0x63, 0x66, 0x65, 0x6c, 0x6f, 0x6a, 0x69, 0x78, 0x7b, 0x7e, 0x7d, 0x74, 0x77, 0x72, 0x71,
        0x50, 0x53, 0x56, 0x55, 0x5c, 0x5f, 0x5a, 0x59, 0x48, 0x4b, 0x4e, 0x4d, 0x44, 0x47, 0x42, 0x41,
        0xc0, 0xc3, 0xc6, 0xc5, 0xcc, 0xcf, 0xca, 0xc9, 0xd8, 0xdb, 0xde, 0xdd, 0xd4, 0xd7, 0xd2, 0xd1,
        0xf0, 0xf3, 0xf6, 0xf5, 0xfc, 0xff, 0xfa, 0xf9, 0xe8, 0xeb, 0xee, 0xed, 0xe4, 0xe7, 0xe2, 0xe1,
        0xa0, 0xa3, 0xa6, 0xa5, 0xac, 0xaf, 0xaa, 0xa9, 0xb8, 0xbb, 0xbe, 0xbd, 0xb4, 0xb7, 0xb2, 0xb1,
        0x90, 0x93, 0x96, 0x95, 0x9c, 0x9f, 0x9a, 0x99, 0x88, 0x8b, 0x8e, 0x8d, 0x84, 0x87, 0x82, 0x81,
        0x9b, 0x98, 0x9d, 0x9e, 0x97, 0x94, 0x91, 0x92, 0x83, 0x80, 0x85, 0x86, 0x8f, 0x8c, 0x89, 0x8a,
        0xab, 0xa8, 0xad, 0xae, 0xa7, 0xa4, 0xa1, 0xa2, 0xb3, 0xb0, 0xb5, 0xb6, 0xbf, 0xbc, 0xb9, 0xba,
        0xfb, 0xf8, 0xfd, 0xfe, 0xf7, 0xf4, 0xf1, 0xf2, 0xe3, 0xe0, 0xe5, 0xe6, 0xef, 0xec, 0xe9, 0xea,
        0xcb, 0xc8, 0xcd, 0xce, 0xc7, 0xc4, 0xc1, 0xc2, 0xd3, 0xd0, 0xd5, 0xd6, 0xdf, 0xdc, 0xd9, 0xda,
        0x5b, 0x58, 0x5d, 0x5e, 0x57, 0x54, 0x51, 0x52, 0x43, 0x40, 0x45, 0x46, 0x4f, 0x4c, 0x49, 0x4a,
        0x6b, 0x68, 0x6d, 0x6e, 0x67, 0x64, 0x61, 0x62, 0x73, 0x70, 0x75, 0x76, 0x7f, 0x7c, 0x79, 0x7a,
        0x3b, 0x38, 0x3d, 0x3e, 0x37, 0x34, 0x31, 0x32, 0x23, 0x20, 0x25, 0x26, 0x2f, 0x2c, 0x29, 0x2a,
        0x0b, 0x08, 0x0d, 0x0e, 0x07, 0x04, 0x01, 0x02, 0x13, 0x10, 0x15, 0x16, 0x1f, 0x1c, 0x19, 0x1a
    };

    //TODO initialiser le schedulerControl: self.scheduler = SchedAESControl(plaintext,key,self)

    state=vector<vector<int>>(plaintext);
    plainText=vector<vector<int>>(plaintext);
    cipherText=vector<vector<int>>(4,vector<int>(4));
    initRoundKey(key);
    t= vector<vector<int>>(nbRounds+1,vector<int>(4));
}


void SchedAES::initRoundKey(vector<vector<int> > key){

    this->roundKeys.push_back(key);
    for(int k=0;k<nbRounds;k++){
        roundKeys.push_back(vector<vector<int>>(4,vector<int>(4,0)));
    }

}

void SchedAES:: shiftRows(int i){
    for (int cpt=0; cpt<i; cpt++){
        Tools::moveToLeft(state[i]);
    }
}

void SchedAES:: mixColumns(int k, int j,vector<int>R)
{
    int temp0 = state[0][j * (R[0]-(k-1)) + (j+0)%4 * (k-R[0])];
    int temp1 = state[1][j * (R[1]-(k-1)) + (j+1)%4 * (k-R[1])];
    int temp2 = state[2][j * (R[2]-(k-1)) + (j+2)%4 * (k-R[2])];
    int temp3 = state[3][j * (R[3]-(k-1)) + (j+3)%4 * (k-R[3])];
    state[0][j * (R[0]-(k-1)) + (j+0)%4 * (k-R[0])] = mCM2[temp0] ^ mCM3[temp1] ^ temp2 ^temp3;
    state[1][j * (R[1]-(k-1)) + (j+1)%4 * (k-R[1])] = mCM2[temp1] ^ mCM3[temp2] ^ temp3 ^temp0;
    state[2][j * (R[2]-(k-1)) + (j+2)%4 * (k-R[2])] = mCM2[temp2] ^ mCM3[temp3] ^ temp0 ^temp1;
    state[3][j * (R[3]-(k-1)) + (j+3)%4 * (k-R[3])] = mCM2[temp3] ^ mCM3[temp0] ^ temp1 ^temp2;
}

void SchedAES::addRoundKey(int k, int i, int j,vector<int>R){
    if (k == 0){  //# texte initial
        state.at(i).at(j) =  state.at(i).at(j) ^ roundKeys.at(k).at(i).at(j);
    }
    else if (k == nbRounds){  //# dernier tour
        int indice= (((j - i) % 4)+4)%4 ;
        int pos = j * int(bool(((R.at(i) - (k - 1)) * (R.at(i) - (k-2))))) + indice * int(
                    bool(((k - R.at(i)) * (R.at((i)) - (k - 2))))) +
                (((indice - i) % 4)+4)%4 * int(bool(((k - R.at(i)) * (R.at(i) - (k - 1)))));

        cipherText[i][pos] = state[i][j] ^ roundKeys[k][i][pos];

    }
    else{
        int indice= (((j - i) % 4)+4)%4 ;
        state[i][j] = (state[i][j] ^ roundKeys[k][i][
                       j * (R.at(i) - (k - 1)) + indice* (k - R.at(i))]);
    }
}

int SchedAES:: getNbLines(){
    return nbLines;
}

int SchedAES:: getNbColumn(){
    return nbCol;
}

int SchedAES:: getNbRounds(){
    return nbRounds;
}


vector<vector<int>> SchedAES::getCipherText(){
    return vector<vector<int>>(cipherText);
}




vector<int> SchedAES::secMult(vector<int>& a,vector<int>& b){
    vector<vector<int>> r(a.size(),vector<int>(a.size(),0));
    vector<int> c(a.size(),0);
    srand (time(NULL));
    for(unsigned i=0;i<a.size();i++){
        for(unsigned j=i+1;j<a.size();j++){
             r.at(i).at(j)=rand()%256; //à vérifier
             r.at(j).at(i)=(r.at(i).at(j)^(gmul(a.at(i),b.at(j))))^(gmul(a.at(j),b.at(i)));
        }
    }
    for(unsigned i=0;i<a.size();i++){
        c.at(i)=gmul(a.at(i),b.at(i));
        for(unsigned j=0;j<a.size();j++){
            if(j!=i){
                c.at(i)=c.at(i)^r.at(i).at(j);
            }
        }
    }
    return c;
}

 void SchedAES::refreshMask(vector<int>& x){
     srand (time(NULL));
     for(unsigned i=1;i<x.size();i++){
        int tmp=rand()%256;//de 0 à 255
        x.at(0)=x.at(0)^tmp;
        x.at(i)=x.at(i)^tmp;
     }

 }

 vector<int> SchedAES::secExp254(vector<int>& x){
     vector<int>z(x.size());
     vector<int>y(x.size());
     vector<int>w(x.size());
     for(unsigned i=0;i<x.size();i++){ //z<-x²
             z.at(i)=gmul(x.at(i),x.at(i));
     }
     refreshMask(z);
     y=secMult(z,x);//x^3
     for(unsigned i=0;i<w.size();i++){ //
         w.at(i)=gmul(y.at(i),y.at(i)); //x^12
         w.at(i)=gmul(w.at(i),w.at(i));
     }
     refreshMask(w);

     y=secMult(y,w);//y^15
     for(unsigned i=0;i<y.size();i++){ //y^240
         y.at(i)=gmul(y.at(i),y.at(i));//y²
         y.at(i)=gmul(y.at(i),y.at(i));//y^4
         y.at(i)=gmul(y.at(i),y.at(i));//y^8
         y.at(i)=gmul(y.at(i),y.at(i));//y^16
     }

     y=secMult(y,w);//x^252
     y=secMult(y,z);//x^254

     return y;
 }

 vector<int> SchedAES::secSbox(vector<int> x){
     vector<int>y=secExp254(x);
     for(unsigned i=0;i<y.size();i++){
         y.at(i)=af(y.at(i));
     }
     if((y.size())%2==0)//la taille est toujours de d+1
         y.at(0)^=(0x63);
     return y;
 }

 int SchedAES::af(int x){
     vector<bool>b=intToBit(x);
     vector<int>b1(8); //à transformer en bit
     vector<bool>c = {1,1,0,0,0,1,1,0}; // 0x63 ici j'ai renversé le byte de maniere a ce que les indices correspondent au bit

     for(int i=0;i<8;i++){
         b1[i] = b[i] ^ b[(i+4) % 8];
         b1[i] ^= b[(i+5) % 8];
         b1[i] ^= b[(i+6) % 8];
         b1[i] ^= b[(i+7) % 8];
         b1[i] ^= c[i];//on ajoute les 63
       }
     int ret=0;
     for(int i=0;i<8;i++){
         ret+=b1.at(i)*pow(2,i);
     }
     return ret;

 }

 vector<bool> SchedAES::intToBit(int b){
     vector<bool> ret;

    for(int i=7;i>=0;i--){
         ret.insert(ret.end(),b%2);
         b=b/2;
     }
     if(ret.size()>8)
         cout<<"attentiooon"<<endl;
     return ret;
 }


 int SchedAES::getElt(int i, int j,int k,bool isKe){
     if(!isKe){
         return state.at(i).at(j);
     }else{
         return roundKeys.at(k).at(i).at(j);
     }

 }
void SchedAES::setElt(int i, int j, int elt,int k,bool isKe){
    if(!isKe){
        state.at(i).at(j)=elt;
    }else {
        t.at(k).at(i)=elt;
    }
}


 bool SchedAES::secKeyExpansion(int k, int i, int j){

         //a ce stade secSbox doit deja avoir eu lieu.( le calcul de sbox est lancé depuis le superScheduler)
         if(j==0){

             if(i==0 && indiceShare==0)
                roundKeys.at(k).at(i).at(j)=roundKeys.at(k-1).at(i).at(j)^t.at(k-1).at((i+1)%nbLines)^rcon.at(k);
             else
                roundKeys.at(k).at(i).at(j)=roundKeys.at(k-1).at(i).at(j) ^ t.at(k-1).at((i+1)%nbLines);

         }else{
             roundKeys[k][i][j] = roundKeys[(k-1)][i][j] ^ roundKeys[k][i][j-1];
         }

         return true;

 }

 /* Multiply two numbers in the GF(2^8) finite field defined
  * by the polynomial x^8 + x^4 + x^3 + x + 1 = 0
  * using the Russian Peasant Multiplication algorithm
  * (the other way being to do carry-less multiplication followed by a modular reduction)
  */
 uint8_t SchedAES::gmul(uint8_t a, uint8_t b) {
     uint8_t p = 0; /* the product of the multiplication */
     while (b) {
             if (b & 1) /* if b is odd, then add the corresponding a to p (final product = sum of all a's corresponding to odd b's) */
                 p ^= a; /* since we're in GF(2^m), addition is an XOR */

             if (a & 0x80) /* GF modulo: if a >= 128, then it will overflow when shifted left, so reduce */
                 a = (a << 1) ^ 0x11b; /* XOR with the primitive polynomial x^8 + x^4 + x^3 + x + 1 (0b1_0001_1011) -- you can change it but it must be irreducible */
             else
                 a <<= 1; /* equivalent to a*2 */
             b >>= 1; /* equivalent to b // 2 */
     }
     return p;
 }
