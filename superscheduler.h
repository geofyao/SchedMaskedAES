#ifndef SUPERSCHEDULER_H
#define SUPERSCHEDULER_H
#include<vector>
#include <string>
#include <map>

class SchedAES;
class SuperScheduler
{
public:
    SuperScheduler();
    SuperScheduler(std::vector<std::vector<int>> plaintext , std::vector<std::vector<int>> key,unsigned order);
    std::vector<SchedAES*> allMatrices;
    void execute();
    void step();
    std::vector<std::vector<int>> displayFinalCipher();

private:
    unsigned d,nbLines,nbCol,nbRounds;
    std::ofstream* file;
    std::vector<std::vector<std::vector<int > > > M ;
    std::vector<std::vector<int >>SB;
    std::vector<int>  R;
    std::vector<std::vector<int > > C;
    std::vector<std::vector<std::vector<int > >>K;
    std::vector<std::string>activeSet;
    std::vector<std::string> keySet;
    std::map<int,bool> keSbReady;
    std::vector<std::map<int,std::vector<bool>>> previousOk;//permet de savoir si on a terminer le calcul de la KE pour la Colonne précédente
    std::vector<std::vector<std::vector<int>>> setAllShares(std::vector<std::vector<int>> matrice);
    std::vector<std::vector<int>> createRandomShare(int high=256,int low=0);
    std::string pick_ActionR();
    void perform_ActionAndUpdate(std::string action);
    void keUpdate(int matrice,int k, int i, int j);
    void sbUpdate(int k, int i, int j);
    void arkUpdate(int k, int i, int j);
    void mcUpdate(int matrice,int k, int j);
    void srUpdate(int k, int i);
    bool onlyKEinActiveSet();
    bool finish();
    void addAction(std::string action,bool newAction);
    void doSuByte(int i, int j, int k,bool isKe);
    void sbPerformed(int k);
    bool isPreviousReady(int k);
    void updateOpList(std::vector<std::string>&,std::vector<std::string>&,std::string,std::string);
    void computeKeySchedule();
};

#endif // SUPERSCHEDULER_H
