#ifndef TOOLS_H
#define TOOLS_H
#include <vector>
#include <string>

#include <map>
class Tools
{
public:
    Tools();

        static int split(std::vector<std::string>& vecteur, std::string chaine, char separateur);
          static  void moveToLeft(std::vector<int>& vecteur);
          static int myrandom (int i);
           static void exportTasksToFile(std:: ofstream* fichier,std::string str);
           static void exportTasksToFile(std:: ofstream* fichier, std::vector<std::vector<std::vector<int>>> vec,std::string title);
           static void exportTasksToFile(std:: ofstream* fichier, std::vector<int> vec,std::string title);
           static void exportTasksToFile(std:: ofstream* fichier, std::vector<std::string> vec,std::string title);
           static void exportTasksToFile(std::ofstream *fichier, std::vector<std::vector<int>> vec,std::string title);
           static std::ofstream openMyFile(std::string file);
           static void closeFile(std::ofstream *fichier);
           static std::map<std::string,std::vector<std::vector<int>>>  readFile(std::string fileName);
           static void myShuffle(std::vector<std::string> &array);
           static bool check(const std::vector<std::string>& vec, std::string str);

};

#endif // TOOLS_H
